import { DataTypes } from "sequelize"
import { sequelize }  from "../database/db.js"


export const Rol = sequelize.define('Roles', {

    id_rol: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre_rol: {
        type: DataTypes.STRING(50),
        unique: true
    },

}, {
    timestamps: false
});
