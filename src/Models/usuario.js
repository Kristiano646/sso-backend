import { DataTypes } from "sequelize";
import { sequelize } from "../database/db.js";

export const Usuario = sequelize.define(
  "Usuarios",
  {
    id_usuario: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    nombre_usuario: {
        type: DataTypes.STRING(100),
        unique: true,
    },

    contraseña: {
        type: DataTypes.STRING(20),
    },
  },
  {
    timestamps: false,
  }
);

