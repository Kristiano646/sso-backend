import { DataTypes } from "sequelize"
import { sequelize }  from "../database/db.js"

export const Accidente = sequelize.define('Accidentes', {

    id_accidente: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    id_evento:{
        type: DataTypes.INTEGER,
    },

    fallecimiento_empleado:{
        type: DataTypes.STRING(300),
    },

    incapacidad_empleado:{
        type: DataTypes.STRING(300),

    },

    lugar_accidente:{
        type: DataTypes.STRING(300),
    },

    accidente_transito:{
        type: DataTypes.STRING(300),
    },

    lesiones_empleado:{
        type: DataTypes.STRING(300),
    },

    lugar_traslado:{
        type: DataTypes.STRING(300),
    }

}, {
    timestamps: true
});
