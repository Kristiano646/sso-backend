import { DataTypes } from "sequelize"
import { sequelize } from "../database/db.js"

export const Formulario = sequelize.define("formulario_seguridad_salud", {
    id_formulario_seguridad_salud:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    formulario:{
        type: DataTypes.STRING(250)
    },

    porcentaje_cumplimiento:{
        type: DataTypes.FLOAT
    }
},{
    timestamps: true
});