import { DataTypes } from "sequelize"
import { sequelize }  from "../database/db.js"

export const Area = sequelize.define('Areas', {

    id_area: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre_area: {
        type: DataTypes.STRING(50),
    },

}, {
    timestamps: false
});

