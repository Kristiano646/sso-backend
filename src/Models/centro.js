import { DataTypes } from "sequelize"
import { sequelize }  from "../database/db.js"

export const Centro = sequelize.define('Centros', {

    id_centro: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre_centro: {
        type: DataTypes.STRING(50),
    },
    direccion_centro: {
        type: DataTypes.STRING(100),
    },
}, {
    timestamps: false
});