//Hector

import { DataTypes } from "sequelize"
import { sequelize } from "../database/db.js"

export const Valoracion = sequelize.define('Valoraciones',{
    id_valoracion: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    
    cedula_empleado: {
        type: DataTypes.STRING(11)
    },

    valor: {
        type: DataTypes.STRING(100)
    }
},{
    timestamps: true
});