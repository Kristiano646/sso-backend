//Hector

import { DataTypes } from "sequelize"
import { sequelize } from "../database/db.js"

export const Evento = sequelize.define("Eventos", {
    id_evento:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    cedula_empleado:{
        type: DataTypes.STRING(11)
    },

    cedula_medico:{
        type: DataTypes.STRING(11)
    },

    cedula:{
        type: DataTypes.STRING(11)
    },

    nom_dia:{
        type: DataTypes.STRING(50)
    },

    fecha_evento:{
        type: DataTypes.DATE
    },

    hora_evento:{
        type: DataTypes.TIME
    },

    ciudad_evento:{
        type: DataTypes.STRING(50)
    },

    provincia_evento:{
        type: DataTypes.STRING(50)
    },

    direccion_evento:{
        type: DataTypes.STRING(100)
    },

    referencia_evento:{
        type: DataTypes.STRING(300)
    },

    sector_evento:{
        type: DataTypes.STRING(100)
    }
},{
    timestamps: true
});