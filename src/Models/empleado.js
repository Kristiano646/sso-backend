import { DataTypes } from "sequelize"
import { sequelize }  from "../database/db.js"

export const Empleado = sequelize.define('Empleado', {

    cedula_empleado: {
        type: DataTypes.STRING(10),
        primaryKey: true,
        unique: true
    },
    nombres_empleado: {
        type: DataTypes.STRING(50),
    },
    apellidos_empleado: {
        type: DataTypes.STRING(50),
    },
    fecha_nac_empleado: {
        type: DataTypes.DATE,
    },
    edad_empleado: {
        type: DataTypes.INTEGER,
    },
    genero_empleado: {
        type: DataTypes.STRING(1),
    },
    ciudad_empleado: {
        type: DataTypes.STRING(50),
    },
    provincia_empleado: {
        type: DataTypes.STRING(50),
    },
    direccion_empleado: {
        type: DataTypes.STRING(100),
    },
    telefono_empleado: {
        type: DataTypes.STRING(10),
    },
    email_empleado: {
        type: DataTypes.STRING(50),
    },
    discapacidad_empleado: {
        type: DataTypes.STRING(100),
    },
    tipo_discapacidad_empleado: {
        type: DataTypes.STRING(50),
    },
    departamento_empleado: {
        type: DataTypes.STRING(50),
    },
    direccion_puesto_empleado: {
        type: DataTypes.STRING(100),
    },
    aptitud_laboral_empleado: {
        type: DataTypes.STRING(50),
    },
}, {
    timestamps: true
});
