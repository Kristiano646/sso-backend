import app from './app.js'
import {sequelize} from './database/db.js'
import * as dotenv from 'dotenv'; 

dotenv.config();

const port = process.env.PORT;
// funcion de incio del servidor 
async function main () {
    try {
        //incia la conxion a la base de datos
        await sequelize.sync({force: false});
        console.log('Connection has been established successfully.');
        //incia el servidor 
        app.listen( port);
        console.log("Server is listening on port " + port );
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

main();