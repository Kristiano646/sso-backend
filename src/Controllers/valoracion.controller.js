//Hector

import { Valoracion } from "../models/valoracion.js";
import "../Database/relaciones.js";

//CRUD BASICO PARA LA VALORACIONES

//Obtener lista de valoraciones

export const getAllValoraciones = async (req, res) => {
    try {
        const allValoraciones = await Valoracion.findAll();
        res.json(allValoraciones);
        console.log("Mostrando todas las valoraciones registradas....");
    } catch (error) {
        return res.status(500).json({message: error.message});

    }
};

//Obtener un evento en especifico
export const getValoracionId = async (req, res) => {
    try {
        const {cedula_valoracion} =req.params;

        const oneValoracion = await Valoracion.findAll({
            where:{cedula_valoracion}
        });

        if(!oneValoracion)
            return res.status(404).json({message: "Valoracion no registrado"});
        res.json(oneValoracion);

    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

//crear una valoracion
export const createValoracion = async (req, res) => {
    const {cedulaValoracion, valuacion} = req.body;

    try {
        const nuevaValoracion = await Valoracion.create({
            cedula_valoracion: cedulaValoracion,
            valor: valuacion
        });

        res.json(nuevaValoracion);
        console.log("Nueva Valoracion Agregada");
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};

//Actualizar Valoracion

export const updateValoracion= async(req, res) =>{
    try {
        const {cedula_valoracion} = req.params;
        const {cedulaValoracion, valuacion} = req.body;

        const valoracionActualizacion = await Valoracion.findOne({
            where:{cedula_valoracion}
        });

        valoracionActualizacion.cedula_valoracion = cedulaValoracion,
        valoracionActualizacion.valor = valuacion

        await valoracionActualizacion.save();
        res.json(valoracionActualizacion);
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};

//Eliminar una valoracion
export const deleteValoracion = async(req, res) =>{
    try {
        const {cedula_valoracion} = req.params;
        await Valoracion.destroy({
            where: {
                cedula_valoracion,
            }
        });
        res.sendStatus(204);
    } catch (error) {
        return res.status(500).json({ message: error.message });

    }
};