//importar los modelas y sus relaciones

import { Usuario } from "../models/usuario.js";
import { Rol } from "../models/rol.js";
import "../database/relaciones.js";

//CRUD basico para el modelo Usuario

// Obtener la lista de usuarios
export const getAllUsers = async (req, res) => {
  try {
    const allUsers = await Usuario.findAll({ include: Rol });
    res.json(allUsers);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Obtener un usuario en especifico
export const getUserById = async (req, res) => {
  try {
    const { id } = req.params;
    const oneUser = await Usuario.findOne({
      where: { id_usuario: id}, 
      include:  [ Rol ]
    });
    if (!oneUser)
      return res.status(404).json({ message: "Usuario no registrado" });
    res.json(oneUser);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Crear un usuario
export const createUser = async (req, res) => {
  const { contraseña, nombreUsuario, nombreRol } = req.body;
  try {
    const nuevoUsuario = await Usuario.create({
      nombre_usuario: nombreUsuario,
      contraseña: contraseña,
    });

    const defaultRol = await Rol.findOne({
      where: { nombre_rol: nombreRol },
    });
    if (!defaultRol)
      return res.status(404).json({ message: "Rol no registrado" });
    await nuevoUsuario.addRoles(defaultRol);
    res.json(nuevoUsuario);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Actualizar un usuario
export const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { newNombre, newContraseña } = req.body;
    const usuarioActualizado = await Usuario.findOne({
      where: { id_usuario: id },
      include: [ Rol ]
    });
    usuarioActualizado.nombre_usuario = newNombre;
    usuarioActualizado.contraseña = newContraseña;
    await usuarioActualizado.save();
    res.json(usuarioActualizado);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Borrar un usuario
export const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;
    await Usuario.destroy({
      where: {
        id_usuario: id,
      },
    });
    res.sendStatus(204);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

//Agregar un nuevo rol a un usuario existente
export const addNewRol = async (req, res) => {
  const { nombreUsuario, nombreRol } = req.body;
  try {
    const oneUser = await Usuario.findOne({
      where: { nombre_usuario: nombreUsuario },
    });
    if (!oneUser)
      return res.status(404).json({ message: "Usuario no registrado" });

    const newRol = await Rol.findOne({
      where: { nombre_rol: nombreRol },
    });
    if (!newRol)
      return res.status(404).json({ message: "Rol no registrado" });
    await oneUser.addRoles(newRol);
    await oneUser.save();
    res.json(oneUser);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

