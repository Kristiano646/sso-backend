import pool from '../Database/pool.js'

// Info general de formulario: Nombre del centro, porcentaje de cumplimiento y ultima persona que edito el formulario
export const obtenerInfoGeneralFormularios = async (req, res, next) => {
    try {
        const areas = await pool.query(
            `SELECT
                form.id_formulario_seguridad_salud as id,
                cent.nombre_centro,
                form.porcentaje_cumplimiento,
                COALESCE((
                    SELECT "Empleados".nombres_empleado || ' ' || "Empleados".apellidos_empleado AS nombres
                    FROM llenas JOIN "Usuarios" ON "Usuarios".id_usuario = llenas.id_usuario
                    JOIN "Empleados" ON "Empleados".cedula_empleado = "Usuarios".cedula_empleado	
                    WHERE llenas.id_formulario_seguridad_salud = form.id_formulario_seguridad_salud
                    ORDER BY llenas.hora_fecha DESC
                    LIMIT 1
                ), '-') as ultimo 
            FROM formulario_seguridad_saluds as form
            JOIN "Centros" as cent ON cent.id_centro = form.id_centro;
            `
        )
        res.json(areas.rows)
    } catch (error) {
        next(error)
    }
}

export const obtenerPCxArea = async (req, res, next) => {
    try {
        const { idForm } = req.params
        const form_areas = await pool.query(`
        SELECT
            "Areas".id_area,
            "Areas".nombre_area,
            cont.porcentaje_cumplimiento
        FROM formulario_seguridad_salud_conts as cont
            JOIN "Areas" ON "Areas".id_area = cont.id_area
        WHERE cont.id_formulario_seguridad_salud = $1;
        `, [idForm])

        if (form_areas.rows.length === 0)
            return res.status(404).json({ message: "Formulario no encontrado" })

        res.json(form_areas.rows)
    } catch (error) {
        next(error)
    }
}

export const obtenerFormxID = async (req, res, next) => {
    try {
        const { idForm } = req.params
        const form = await pool.query(`
        SELECT
	        form.id_centro,
	        "Centros".nombre_centro,
	        form.formulario,
	        form.porcentaje_cumplimiento
        FROM formulario_seguridad_saluds as form
	        JOIN "Centros" ON "Centros".id_centro = form.id_centro
        WHERE form.id_formulario_seguridad_salud = $1;
        `, [idForm])

        if (form.rows.length === 0)
            return res.status(404).json({ message: "Formulario no encontrado" })

        res.json(form.rows[0])
    } catch (error) {
        next(error)
    }
}

export const obtenerRegistros = async (req, res, next) => {
    try {
        const { idForm } = req.params
        const registros = await pool.query(`
        SELECT
            llenas.id_registro,
		    emp.nombres_empleado || ' ' || emp.apellidos_empleado AS nombres,
		    DATE(llenas.hora_fecha) as fecha,
		    llenas.hora_fecha::time as hora,
		    llenas.comentario
	    FROM llenas
			JOIN "Usuarios" as usr ON llenas.id_usuario = usr.id_usuario
		    JOIN "Empleados" as emp ON emp.cedula_empleado = usr.cedula_empleado
	    WHERE llenas.id_formulario_seguridad_salud = $1
	    ORDER BY llenas.hora_fecha DESC;
        `, [idForm])

        if (registros.rows.length === 0)
            return res.status(200).json([{ 
                id_registro: "-",
                nombres: "-",
                fecha: "-",
                hora: "-",
                comentario: "-",
            }])

        res.json(registros.rows)
    } catch (error) {
        next(error)
    }
}

export const guardarForm = async (req)=>{
    try {
        const { idForm } = req.params
        const {formulario}=req.body
        
        const form = await pool.query(`
        UPDATE formulario_seguridad_saluds
        SET formulario = $1
	    WHERE id_formulario_seguridad_salud = $2
	    `,[formulario, idForm])
    } catch (error) {
        next(error)
    }
}