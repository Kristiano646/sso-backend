//Hector

import { Evento } from "../models/evento.js";
import "../database/relaciones.js";
 
//CRUD BASICO PARA EL MODELO EVENTO

//Obtener lista de eventos
export const getAllEventos = async (req, res) => {
    try {
        const allEventos = await Evento.findAll();
        res.json(allEventos);
        console.log("Monstrando todos los eventos registrados....");

    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

//Obtener un evento en especifico
export const getEventoById = async (req, res) => {
    try {
        const {cedula_empleado} = req.params;
        //revisar esta linea de codigo con previo aviso :v
        const oneEvent = await Evento.findAll({
            where: {cedula_empleado}
        });

        if (!oneEvent)
            return res.status(404).json({message: "Evento no registrado"});
        res.json(oneEvent);
    } catch (error) {
        return res.status(500).json({message: error.message});
    }
};

//Crear un evento
export const createEvento = async (req, res) => {
    const {cedulaEmpleado, cedulaMedico, Cedula, nomDia, fechaEvento, horaEvento, ciudadEvento, provinciaEvento, direccionEvento, referenciaEvento, sectorEvento} = req.body;

    try {
        const nuevoEvento = await Evento.create({
            //base  -- recibe
            cedula_empleado: cedulaEmpleado,
            cedula_medico: cedulaMedico,
            cedula: Cedula,
            nom_dia: nomDia,
            fecha_evento: fechaEvento,
            hora_evento: horaEvento,
            ciudad_evento: ciudadEvento,
            provincia_evento: provinciaEvento,
            direccion_evento: direccionEvento,
            referencia_evento: referenciaEvento,
            sector_evento: sectorEvento
        });

        res.json(nuevoEvento);
        console.log("Nuevo Evento Registrado");

    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};

//Actualizar Evento
export const updateEvento = async (req, res) => {
    try {
        const { cedula_empleado } = req.params;
        const {cedulaEmpleado, cedulaMedico, Cedula, nomDia, fechaEvento, horaEvento, ciudadEvento, provinciaEvento, direccionEvento, referenciaEvento, sectorEvento} = req.body;

        const eventoActualizado = await Evento.findOne({
            where:{ cedula_empleado }
        });
        eventoActualizado.cedula_empleado = cedulaEmpleado,
        eventoActualizado.cedula_medico = cedulaMedico,
        eventoActualizado.cedula = Cedula,
        eventoActualizado.nom_dia = nomDia,
        eventoActualizado.fecha_evento = fechaEvento,
        eventoActualizado.hora_evento = horaEvento,
        eventoActualizado.ciudad_evento = ciudadEvento,
        eventoActualizado.provincia_evento = provinciaEvento,
        eventoActualizado.direccion_evento = direccionEvento,
        eventoActualizado.referencia_evento = referenciaEvento,
        eventoActualizado.sector_evento = sectorEvento
        
        await eventoActualizado.save();
        res.json(eventoActualizado);
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};

//Borar Evento
export const deleteEvento = async (req, res) => {
    try {
        const { cedula_empleado } = req.params;
        await Evento.destroy({
            where: {
                cedula_empleado,
            }
        });
        res.sendStatus(204);
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
};