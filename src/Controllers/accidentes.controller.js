import { Accidente } from "../models/accidente.js";

//CRUD basico para el modelo Accidente

// Obtener la lista de Accidentes
export const getAllAccidentes = async (req, res) => {
  try {
    const allAccidente = await Accidente.findAll();
    res.json(allAccidente);
    console.log("Mostrando Accidentes resgistrados...");
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Obtener un Accidente en especifico
export const getAccidenteById = (req, res) => {
  res.send("getting of Accidentes");
};

export const createAccidente = async (req, res) => {
  // Espera recibir un paramentro "nombre_accidente" para crear el Accidente
  const { nombre_acidente } = req.body;
  const { id_evento } = req.id_evento;
  const { fallecimiento_empleado } = req.fallecimiento_empleado;
  const { incapacidad_empleado } = req.incapacidad_empleado;
  const { lugar_accidente } = req.lugar_accidente;
  const { accidente_transito } = req.accidente_transito;
  const { lesiones_empleado } = req.incapacidad_empleado;
  const { lugar_traslado } = req.lugar_accidente;


  try {
    // Creando un nuevo objeto Accidente con el metodo create
    const nuevoAccidente = await Accidente.create({
      id_evento,
      fallecimiento_empleado,
      incapacidad_empleado,
      lugar_accidente,
      accidente_transito,
      lesiones_empleado,
      lugar_traslado,
      nombre_Accidente,
    });
    res.json(nuevoAccidente);
    console.log("Nuevo accidente creado");
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Actualizar un Area
export const updateAccidente = (req, res) => {
  res.send("getting of accidentr");
};

// Borrar un area
export const deleteAccidente = (req, res) => {
  res.send("getting of accidentr");
};
