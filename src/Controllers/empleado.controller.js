import { Empleado } from "../models/empleado.js"

//CRUD basico para el modelo empleado

// Obtener la lista de empleados 
export const getAllEmpleado  = async (req, res) => {
    try {
        const allEmpleado =  await Empleado.findAll();
        res.json(allEmpleado);
        console.log("Mostrando empleado resgistrados...");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
};

// Obtener un empleado en especifico 
export const getEmpleadoById  = (req, res) => {
    res.send('getting of empleados')
};


export const createEmpleado  = async (req, res) => {

    // Espera recibir un paramentro "ceduLA_EMPLEADO" para crear el rol
    const { cedula_empleado } = req.body;

    try {
        // Creando un nuevo objeto empleado con el metodo create
        const nuevoEmpleado=  await Empleado.create({
            cedula_empleado
        });
        res.json(nuevoEmpleado);
        console.log("Nuevo empleado creado");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
   
};

// Actualizar un emplado
export const updateEmpleado  = (req, res) => {
    res.send('getting of emplado')
};

// Borrar un emplado
export const deleteEmpleado = (req, res) => {
    res.send('getting of emplado')
};
