import { Centro } from "../models/centro.js"

//CRUD basico para el modelo Centro

// Obtener la lista de centros 
export const getAllCentro  = async (req, res) => {
    try {
        const allCentro =  await Centro.findAll();
        res.json(allCentro);
        console.log("Mostrando centros resgistrados...");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
};

// Obtener un CENTRO en especifico 
export const getCentroById  = (req, res) => {
    res.send('getting of centros')
};


export const createCentro  = async (req, res) => {

    // Espera recibir un paramentro "nombre" para crear el rol
    const { nombre_centro} = req.body;

    try {
        // Creando un nuevo objeto rol con el metodo create
        const nuevoCentro =  await Centro.create({
            nombre_centro
        });
        res.json(nuevoCentro);
        console.log("Nuevo centro creado");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
   
};

// Actualizar un centro
export const updateCentro  = (req, res) => {
    res.send('getting of centro')
};

// Borrar un centro
export const deleteCentro = (req, res) => {
    res.send('getting of centro')
};
