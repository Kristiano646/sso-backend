import { Rol } from "../models/rol.js"
//CRUD basico para el modelo ROL

// Obtener la lista de roles 
export const getAllRol  = async (req, res) => {
    try {
      //Se obtiene una lsita con todos los roles registrados 
        const allRol =  await Rol.findAll();
        res.json(allRol);
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
};

// Obtener un rol en especifico 
export const getRolById  = async (req, res) => {
    try {
       //se recibe el parametro id para realizar la busqueda
        const { id } = req.params;
        const oneRol =  await Rol.findOne({
            where: { id_rol: id },
        });
        if(!oneRol) return res.status(404).json({message: "Rol no registrado"});
        res.json(oneRol);
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
};

// Crear un nuevo rol
export const createRol = async (req, res) => {
  // Espera recibir un paramentro "nombre" para crear el rol
  const { nombre_rol } = req.body;
  try {
    // Creando un nuevo objeto rol con el metodo create
    const nuevoRol = await Rol.create({
      nombre_rol,
    });
    res.json(nuevoRol);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Actualizar un rol
export const updateRol = async (req, res) => {
  try {
    const { id } = req.params;
    const { nombre } = req.body;
    const rolActualizado = await Rol.findByPk(id);
    rolActualizado.nombre_rol = nombre;
    await rolActualizado.save();
    res.json(rolActualizado);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

// Borrar un rol
export const deleteRol =  async (req, res) => {

   try {
    const { id } = req.params;
    await Rol.destroy({ 
        where: {
            id_rol: id,
        },
    });
    res.sendStatus(204);
   } catch (error) {
    return res.status(500).json({ message: error.message });
   }
};
