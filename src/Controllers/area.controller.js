import { Area } from "../models/area.js"

//CRUD basico para el modelo Area

// Obtener la lista de areas 
export const getAllAreas  = async (req, res) => {
    try {
        const allArea =  await Area.findAll();
        res.json(allArea);
        console.log("Mostrando areas resgistrados...");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
};

// Obtener un area en especifico 
export const getAreaById  = (req, res) => {
    res.send('getting of areas')
};


export const createArea  = async (req, res) => {

    // Espera recibir un paramentro "nombre_area" para crear el area
    const { nombre_area } = req.body;

    try {
        // Creando un nuevo objeto area con el metodo create
        const nuevoArea =  await Area.create({
        nombre_area
        });
        res.json(nuevoArea);
        console.log("Nuevo area creado");
    } catch (error) {
        return res.status(500).json({message:error.message});
    }
   
};

// Actualizar un Area
export const updateArea  = (req, res) => {
    res.send('getting of areas')
};

// Borrar un area
export const deleteArea = (req, res) => {
    res.send('getting of areas')
};
