import {Router} from 'express'
import { getAllCentro, getCentroById , createCentro , updateCentro , deleteCentro } from '../controllers/centro.controller.js'

const router =  Router ();


//Rutas de conexion al controlador Centro
router.get('/centro', getAllCentro)
router.post('/centro', createCentro)
router.put('/centro/:id', updateCentro)
router.delete('/centro/:id', deleteCentro)
router.get('/centro/:id', getCentroById)

export default router