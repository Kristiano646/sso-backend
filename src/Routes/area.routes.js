import {Router} from 'express'
import { getAllAreas, getAreaById , createArea , updateArea , deleteArea } from '../Controllers/area.controller.js'

const router =  Router ();


//Rutas de conexion al controlador Area
router.get('/area', getAllAreas)
router.post('/area', createArea)
router.put('/area/:id', updateArea)
router.delete('/area/:id', deleteArea)
router.get('/area/:id', getAreaById)

export default router