import { Router } from 'express'
import {getAllEventos, getEventoById, createEvento, updateEvento, deleteEvento} from '../controllers/evento.controller.js'


const router = Router();

//rutas de conexion al controlador Evento

router.get('/eventosListados', getAllEventos);
router.post('/evento', createEvento);
router.put('/evento/:cedula_empleado', updateEvento);
router.delete('/evento/:cedula_empleado', deleteEvento);
router.get('/evento/:cedula_empleado', getEventoById);

export default router