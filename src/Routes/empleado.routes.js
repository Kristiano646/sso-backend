import {Router} from 'express'
import { getAllEmpleado, getEmpleadoById , createEmpleado , updateEmpleado , deleteEmpleado } from '../controllers/empleado.controller.js'

const router =  Router ();


//Rutas de conexion al controlador Empleado
router.get('/rol', getAllEmpleado)
router.post('/rol', createEmpleado)
router.put('/rol/:id', updateEmpleado)
router.delete('/rol/:id', deleteEmpleado)
router.get('/rol/:id', getEmpleadoById)

export default router