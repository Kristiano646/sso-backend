import {Router} from 'express'
import { getAllRol, getRolById , createRol , updateRol , deleteRol } from '../controllers/rol.controller.js'

const router =  Router ();


//Rutas de conexion al controlador ROL
router.get('/roles', getAllRol)
router.post('/rol', createRol)
router.put('/rol/:id', updateRol)
router.delete('/rol/:id', deleteRol)
router.get('/rol/:id', getRolById)

export default router