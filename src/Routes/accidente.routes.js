import {Router} from 'express'
import { getAllAccidentes, getAccidenteById , createAccidente , updateAccidente , deleteAccidente } from '../Controllers/accidentes.controller.js'

const router =  Router ();


//Rutas de conexion al controlador Area
router.get('/accidente', getAllAccidentes)
router.post('/accidente', createAccidente)
router.put('/accidente/:id', updateAccidente)
router.delete('/accidente/:id', deleteAccidente)
router.get('/accidente/:id', getAccidenteById)

export default router