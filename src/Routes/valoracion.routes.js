import { Router } from "express"
import {getAllValoraciones, createValoracion, updateValoracion, deleteValoracion, getValoracionId} from '../controllers/valoracion.controller.js'

const router = Router();

router.get('/valoraciones', getAllValoraciones);
router.post('/valoracion', createValoracion);
router.put('/valoracion/:cedula_valoracion', updateValoracion);
router.delete('/valoracion/:cedula_valoracion', deleteValoracion);
router.get('/valoracion/:cedula_valoracion', getValoracionId);

export default router