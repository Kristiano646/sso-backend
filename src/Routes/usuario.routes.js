import {Router} from "express";
import {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    addNewRol
} from "../Controllers/usuario.controller.js";

const router = Router();

// Rutas de conexion al controlador usuario
router.get("/users", getAllUsers);
router.post("/user", createUser);
router.post("/user/addRol", addNewRol);
router.put("/user/:id", updateUser);
router.delete("/user/:id", deleteUser);
router.get("/user/:id", getUserById);
//router.delete("/user/:id/removeRol", removeRol);

export default router;
