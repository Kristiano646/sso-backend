import { Router } from 'express';
import { obtenerInfoGeneralFormularios, obtenerPCxArea, obtenerFormxID, obtenerRegistros, guardarForm  } from '../Controllers/formularios.controller.js'

const router = Router()

router.get('/forms', obtenerInfoGeneralFormularios)

router.get('/form/areas/:idForm', obtenerPCxArea)

router.get('/form/:idForm', obtenerFormxID)

router.get('/form/registros/:idForm', obtenerRegistros)

router.put('/form/:idForm', guardarForm)

export default router;