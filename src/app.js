import express from 'express'
import morgan from 'morgan'
import cors from 'cors'

import rolRoutes from './routes/rol.routes.js'
import userRoutes from './routes/usuario.routes.js'
import eventoRoutes from './routes/evento.routes.js'
import valoracionRoutes from './routes/valoracion.routes.js'
import loginRoutes from './routes/login.routes.js'
import areaRoutes from './routes/area.routes.js'
import accidenteRoutes from './routes/accidente.routes.js'

//Instancias de express para inciar el servidor
const app = express()
app.use(cors())

//dev
app.use(morgan('dev'));

//middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false}));

//Rutas de acceso al area
app.use(areaRoutes);

//Rutas de acceso al login 
app.use(loginRoutes);

//Rutas de acceso Rol
app.use(rolRoutes)

//Rutas de acceso al usuario 
app.use(userRoutes)

//Rutas de acceso a evento 
app.use(eventoRoutes)

//Rutas de acceso a valoracion 
app.use(valoracionRoutes)

//Rutas de acceso a accidentes 
app.use(accidenteRoutes)
//Rutas de acceso a formularios
//app.use(formRoutes)


export default app;